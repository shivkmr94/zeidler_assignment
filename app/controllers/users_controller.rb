class UsersController < ApplicationController
  before_action :set_default_pagination

  def index
    @users = User.search(filtering_params).paginate(page: params[:page], per_page: params[:per_page])
  end

  private

  def filtering_params
    params.fetch(:search, {}).permit(:age, :gender, :country)
  end

  def set_default_pagination
    page = params[:page] || 1
    per_page = params[:per_page] || 10
    params.merge!({page: page, per_page: per_page})
  end

end
