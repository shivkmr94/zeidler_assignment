class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  enum gender: { male: 0, female: 1, transgender: 3 }

  validates :email, 
            :uniqueness => {:message => "Another account is already using this email address"},
            :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i,
            :message=> "Please Check Email Id" },
            :presence =>true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :country, presence: true

  scope :filter_by_country, -> (country) { where country: country }
  scope :filter_by_age, -> (age) { where age: age }

  def self.search(params)
    users = all
    users = users.filter_by_age(params[:age]) if params[:age].present?
    users = users.filter_by_country(params[:country]) if params[:country].present?
    users = users.send(params[:gender]) if params[:gender].present?
    users
  end

  def full_name
    "#{first_name} #{last_name}"
  end
end
