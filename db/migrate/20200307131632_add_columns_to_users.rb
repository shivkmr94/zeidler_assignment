class AddColumnsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :age, :integer
    add_column :users, :gender, :integer
    add_column :users, :country, :string
    
    add_index :users, :age
    add_index :users, :gender
    add_index :users, :country
  end
end
